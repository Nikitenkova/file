<?php
class commands{
    public $data = array();
    public $command = array();

    public function __construct($data,$type){
        $this->data = $data;
        $this->type = $type;

    }

    public function analysisType(){
        switch ($this->type) {
            case 'txt':
                $analysis = new txt_analysis($this->data);
                break;
            case 'json':
                $analysis = new json_analysis($this->data);
                break;
            case 'xml':
                $analysis = new xml_analysis($this->data);
                break;
        }
        return $analysis;
    }

    public function hello(){
        $this->command[1] = 'Hello';
        $this->command[2] = date("H:i:s");
        $this->command[3] = 'Команда hello выполнена';
        return $this->command;
    }

    public function newdate(){
        $this->command[1] = date("d/m/Y");
        $this->command[2] = date("H:i:s");
        $this->command[3] = 'Команда date выполнена';
        return $this->command;
    }

    public function rnd(){
        $this->command[1] = rand();
        $this->command[2] = date("H:i:s");
        $this->command[3] = 'Команда rnd выполнена';
        return $this->command;
    }

    public function add($a,$b){
        $this->command[1] = $a . $b;
        $this->command[2] = date("H:i:s");
        $this->command[3] = 'Команда add выполнена';
        return $this->command;
    }

    public function multy($a,$b){
        $this->command[1] = $a * $b;
        if ($this->command[1] == NULL){
            $this->command[1] = 'Ошибка';
            $this->command[3] = 'Команда multy не может быть выполнена';
        }
        else {
            $this->command[3] = 'Команда multy выполнена';
        }
        $this->command[2] = date("H:i:s");
        return $this->command;
    }

}