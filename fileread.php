<?php
class fileRead{
    public $type = null;
    public $data = array();

    public function __construct($filename) {
        $this->filename = $filename;
    }

    public function type(){
        $this->type = substr($this->filename, (strpos($this->filename, ".") + 1));
        return $this->type;
    }

    public function read(){

        switch ($this->type()) {
            case 'txt':
                $this->data = file($this->filename);
                break;
            case 'json':
                $this->data = file_get_contents($this->filename);
                $this->data = json_decode($this->data);
                break;
            case 'xml':
                $this->data = simplexml_load_file($this->filename);
                break;
        }
        return $this->data;
    }
}