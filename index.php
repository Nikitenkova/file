<html>
<head>
    <title>Файл</title>
    <meta charset="UTF-8">
</head>
<body>
<?php
date_default_timezone_set('Asia/Almaty');
require 'fileRead.php';
require 'commands.php';
require 'txt_analysis.php';
require 'json_analysis.php';
require 'xml_analysis.php';
require 'result.php';
require 'multiply.php';
$new_file1 = new fileread("input.txt");
$commands1 = new multiply($new_file1->read(),$new_file1->type());
$result1 = new result($commands1->realization_multiply());
$result1->show();
$new_file2 = new fileread("input.json");
$commands2 = new multiply($new_file2->read(),$new_file2->type());
$result2 = new result($commands2->realization_multiply());
$result2->show();
$new_file3 = new fileread("input.xml");
$commands3 = new multiply($new_file3->read(),$new_file3->type());
$result3 = new result($commands3->realization_multiply());
$result3->show();
$new_file4 = new fileread("input2.xml");
$commands4 = new multiply($new_file4->read(),$new_file4->type());
$result4 = new result($commands4->realization_multiply());
$result3->show();
?>
</body>
</html>