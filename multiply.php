<?php
class multiply extends commands{
    public $commandmulty = array();
    public $newanalysis = NULL;

    public function realization_multiply(){
        $newanalysis = $this->analysisType();
        $this->data = $newanalysis->analysis();
        foreach ($this->data as $value) {
            if ($value == 'multy') {
                $this->commandmulty = $this->multy($newanalysis->a, $newanalysis->b);
            }
        }
        return $this->commandmulty;
    }

}