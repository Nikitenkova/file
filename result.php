<?php
class result{
    public $result = array();

    public function __construct($result){
        $this->result = $result;
    }

    public function show(){
        foreach ($this->result as $value) {
            echo '<pre>', $value, '</pre>';
        }
    }
}