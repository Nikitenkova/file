<?php
class json_analysis {
    public $a = null;
    public $b = null;

    public function __construct($data){
        $this->data = $data;
    }

    public function analysis(){
                foreach ($this->data->values as $key => $value) {
                    if (!is_object($value)){
                        $this->values[$key] = $value;
                    }
                    else {
                        $this->a = $value->add->a;
                        $this->b = $value->add->b;
                        $this->values[$key] = 'add';
                    }
                }
                $this->data = $this->values;
                return $this->data;
    }
}